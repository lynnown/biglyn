package com.biglyn.dao;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.biglyn.bean.Website;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class WebsiteDao {
    private static JdbcTemplate tpl;
    static {
        try (InputStream is = Website.class.getClassLoader().getResourceAsStream("druid.properties")){
            // 获取连接池
            Properties properties = new Properties();
            properties.load(is);
            DataSource ds = DruidDataSourceFactory.createDataSource(properties);
            //创建tpl
            tpl = new JdbcTemplate(ds);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * 删除
     * @param id
     * @return
     */
    public boolean remove(Integer id){
        return false;
    }

    /**
     * 添加或更新
     * @param website
     * @return
     */
    public boolean save(Website website){
        Integer id = website.getId();
        List<Object> args = new ArrayList<>();
        args.add(website.getFooter());
        String sql = "";
        if (id == null || id < 1){
            sql = "INSERT INTO website(footer) VALUES(?)";
        }else {
            sql = "UPDATE website SET footer = ? WHERE id = ?";
            args.add(id);
        }

        Object[] array = args.toArray();
        System.out.println(args);
        System.out.println(array);
        return tpl.update(sql, args.toArray()) > 0;
    }

    /**
     * 获取单个对象
     * @param id
     * @return
     */
    public Website get(Integer id){
        return null;
    }

    /**
     * 获取多个数据
     * @return
     */
    public List<Website> list(){
        String sql = "SELECT id, created_time, footer FROM website";

//        BeanPropertyRowMapper,将数据库的数据转bean
        List<Website> list = tpl.query(sql, new BeanPropertyRowMapper<>(Website.class));
        return list;
    }

    /**
     * 获取数量 统计值
     * @return
     */
    public Integer count(){
        return 0;
    }
}
