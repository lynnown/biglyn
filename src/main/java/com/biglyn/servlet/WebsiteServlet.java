package com.biglyn.servlet;

import com.biglyn.bean.Website;
import com.biglyn.dao.WebsiteDao;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

//@WebServlet(name = "WebsiteServlet", value = "/WebsiteServlet")
@WebServlet("/website/*")
public class WebsiteServlet extends BaseServlet {
    private WebsiteDao dao = new WebsiteDao();

    public void admin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("website - admin1234567");

        //先获取数据
        List<Website> websites = dao.list();
        Website website = (websites == null || websites.isEmpty()) ? null : websites.get(0);

        //放进request
        request.setAttribute("website",website);

        //request转发 然后拿给jsp展示
        request.getRequestDispatcher("/page/admin/website.jsp").forward(request,response);
    }

    public boolean save(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("点击保存按钮");

        //beanutils将map转bean
        //BeanPropertyRowMapper将数据库获取的内容转bean
        Website website = new Website();
        //jsp过来的website与admin前面传的setAttribute中的website不一定相同,要看<form>里面的value
        BeanUtils.populate(website,request.getParameterMap());
        if (dao.save(website)){//保存成功
            response.sendRedirect(request.getContextPath() + "/website/admin");
        } else {//保存失败
            request.setAttribute("error", "网站信息保存失败");
            request.getRequestDispatcher("/page/error.jsp").forward(request,response);
        }

        return false;
    }
}
