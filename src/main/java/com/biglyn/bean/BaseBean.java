package com.biglyn.bean;
import java.util.Date;

public class BaseBean {
    private Integer id;
    private Date createdTime;//java里驼峰标识 数据库下划线分割

    //Generate com+n 自动生成setter getter方法
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
}
